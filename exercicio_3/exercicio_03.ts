/*3) Média Ponderada

Faça um programa que receba três notas, calcule e mostre a
média ponderada entre elas.
*/

    //primeiro bloco
    //inicio
    namespace exercicio_3
    //entrada
{
    let nota1, nota2, nota3, peso1, peso2, peso3: number;

    nota1 = 10;
    nota2 = 8.5;
    nota3 = 8;
    peso1 = 1;
    peso2 = 3;
    peso3 = 2;

    let media: number

    //processo

    media = (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3)

    //saida

    console.log (`a media do aluno é: ${media}`)
}
    //fim