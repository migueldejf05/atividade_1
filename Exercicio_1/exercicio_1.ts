/* 
1) Soma de 4 números

Faça um programa que receba quatro números inteiros,
calcule e mostre a soma desses números.
*/

//Primeiro bloco
//inicio
namespace exercicio_1

{
    //entrada dos dados 
    //var --- let --- const
    let numero1, numero2, numero3, numero4: number;

    numero1 = 100432; 
    numero2 = 723212;
    numero3 = 434234;
    numero4 = 351;

    let resultado: number;

    //Processar os dados
    resultado = numero1 + numero2 + numero3 + numero4;

    //Saida
    console.log("o resultado da soma é: " + resultado);
    //ou pode-se escrever
    console.log(`o resultado da soma é: ${resultado}`);
}