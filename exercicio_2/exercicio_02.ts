/*2) Média Aritmética

Faça um programa que receba três notas, calcule e mostre a
média aritmética entre elas.
*/

    //primeiro bloco
    //inicio
    namespace exercicio_02

    //entrada
{
    let nota1, nota2 ,nota3: number;

    nota1 = 10;
    nota2 = 10;
    nota3 = 0;

    let resultado: number;

    //processar dados 
    resultado = (nota1 + nota2 + nota3) /3 

    //saida
    console.log(`a media do aluno é: ${resultado}`)
}
    //fim