/*5) Cálculo de salário

Faça um programa que receba o salário de um funcionário e
o percentual de aumento, calcule e mostre o valor do
aumento e o novo salário.
*/

    //primeiro bloco
    //inicio
    namespace exercicio_05
    
{
    //entrada
    let salario: number
    let aumento_salario: number
    let aumento: number
    let novo_salario: number

    
    salario = 11671;
    aumento = 13.5/100;

    //processo

    aumento_salario = salario * aumento
    novo_salario = salario * aumento + salario;

    //saida
    console.log (`o seu salario com aumento sera: ${novo_salario} \no aumento sera de: ${aumento_salario}`)
    
}
    //fim