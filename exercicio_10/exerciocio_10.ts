/*10) Área de um círculo

Faça um programa que calcule e mostre a área de um
círculo. Sabe-se que: Área = π.R2
*/

    //primeiro bloco
    //inicio
    namespace exercicio_10
{
    //entrada
    let pi
    let raio
    let area_circulo

    pi = 3.14
    raio = 6

    //processo
    area_circulo = pi * Math.pow(raio, 2)
    
    //saida
    console.log (`a area do circulo é: ${area_circulo}`)
}
    //fim