
    /*7) Cálculo de salário

Faça um programa que receba o salário-base de um

funcionário, calcule e mostre o seu salário a receber, sabendo-
se que esse funcionário tem gratificação de R$50,00 e paga

imposto de 10% sobre o salário-base.
*/

    //primeiro bloco
    //inicio
    namespace exercicio_07
{
    //entrada 
    let salario_base: number;
    let salario_bonificaçao: number;
    let salario_imposto: number;
    let salario_final: number;

    salario_base = 4800;
    salario_bonificaçao = 50.00;

    //processo
    salario_imposto = salario_base * 10/100;
    salario_final = salario_base + salario_bonificaçao - salario_imposto;

    //saida
    console.log (`o seu salario é: ${salario_final}`);
}
    //fim