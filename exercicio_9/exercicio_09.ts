/*9) Área do triângulo

Faça um programa que calcule e mostre a área de um
triângulo. Sabe-se que: Área = (base * altura)/2.
*/

    //primeiro bloco
    //inicio 

    namespace exercicio_09
{
    let base: number;
    let altura: number;
    let area_triangulo: number;

    base = 6;
    altura = 7;

    //processo
    area_triangulo = (base * altura) / 2

    //saida
    console.log (`a area do tringulo é igual a: ${area_triangulo}`)
}
    //fim