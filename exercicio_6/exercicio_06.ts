/*6) Cálculo de salário

Faça um programa que receba o salário-base de um
funcionário, calcule e mostre o salário a receber, sabendo-se
que esse funcionário tem gratificação de 5% sobre o salário
base e paga imposto de 7% sobre o salário-base.
*/

    //primeiro bloco
    //inicio
    namespace exercicio_06
{
    //entrada 
    let salario_base: number;
    let salario_bonificaçao: number;
    let salario_imposto: number;
    let salario_final: number;

    salario_base = 4800;

    //processo
    salario_bonificaçao = salario_base * 5/100;
    salario_imposto = salario_base * 7/100;
    salario_final = salario_base + salario_bonificaçao - salario_imposto;
    //saida
    console.log (`o seu salario é: ${salario_final}`);
}
    //fim