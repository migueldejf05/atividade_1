/*4) Cálculo de salário

Faça um programa que receba o salário de um funcionário,
calcule e mostre o novo salário, sabendo-se que este sofreu
um aumento de 25%.
*/

    //primeiro bloco
    //inicio
    namespace exercicio_04
{
    //entrada 
    let salario: number;

    salario = 9000;

    let salario_aumento: number

    //processo
    
     salario_aumento = salario * 25/100 + salario
     
     //saida
     console.log (`o novo salario com aumento é: ${salario_aumento}`)

}
    //fim