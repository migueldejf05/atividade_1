/*8) Cálculo de salário

Faça um programa que receba o valor de um depósito e o
valor da taxa de juros, calcule e mostre o valor do rendimento
e o valor total depois do rendimento.
*/

    //primeiro bloco
    //inicio

    namespace exercicio_08
{
    let deposito: number;
    let juros: number;
    let rendimento: number;
    let deposito_com_rendimento: number;
    deposito = 6925;
    juros = 6/100;

    //processo

    rendimento = deposito * juros;
    deposito_com_rendimento = rendimento + deposito;

    //saida
    console.log (` o seu rendimento é: ${rendimento} \n o seu deposito com o rendimento é: ${deposito_com_rendimento}`);

}
    //fim